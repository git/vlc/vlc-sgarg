/*****************************************************************************
 * openal.c: output module to allow OpenAL integration
 *****************************************************************************
 *
 * Authors: Shaurav Garg <shauravg@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>
#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_aout.h>
#include <vlc_charset.h>
#include <vlc_atomic.h>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

/*****************************************************************************
 * Local prototypes.
 *****************************************************************************/
static int  OpenAudio  ( vlc_object_t * );
static void CloseAudio ( vlc_object_t * );
static void Play       ( aout_instance_t * );
static void* OpenALThread (void *);
static void OpenALBuffer (aout_instance_t *);
static int TakeMin(int[] );
static int FindDevicesCallback( vlc_object_t *p_this, char const *psz_name,
                                vlc_value_t newval, vlc_value_t oldval, void *p_unused );
static void GetDevices( vlc_object_t *, module_config_t * );
/*****************************************************************************
 * Used to configure OpenAL
 ****************************************************************************/
#define MAX_CHANS 2
#define NUM_BUF 8
#define CHUNK_SIZE 512
#define BUFFER_SIZE 4096
#define DEFAULT_OPENAL_DEVICE "default"
#define ALC_ALL_DEVICES_SPECIFIER 0x1013

static const char *const ppsz_devices[] = {
    "default", "default",};

static const char *const ppsz_devices_text[] = {
    N_("Default"), N_("Default"),};

static ALuint buffers1[NUM_BUF];
static ALuint buffers2[NUM_BUF];
static ALuint source[MAX_CHANS];
static ALenum openalFormat = AL_FORMAT_STEREO_FLOAT32;
//static ALenum openalFormat = AL_FORMAT_MONO16;
/*****************************************************************************
 * aout_sys_t: directx audio output method descriptor
 *****************************************************************************
 * This structure is part of the audio output thread descriptor.
 * It describes the direct sound specific properties of an audio device.
 *****************************************************************************/
struct aout_sys_t
{
	int freq;
	mtime_t start_date;
	vlc_thread_t thread;
	vlc_sem_t wait;
	vlc_atomic_t abort;
	int i_buffer_size;
	uint8_t *silence_buffer;
};

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
#define DEVICE_TEXT N_("Output device")
#define DEVICE_LONGTEXT N_("Select your audio output device")

vlc_module_begin ()
    set_description( N_("OpenAL audio output") )
    set_shortname( "OpenAL_SPAT" )
    set_capability( "audio output", 100 )
    set_category( CAT_AUDIO )
    set_subcategory( SUBCAT_AUDIO_AOUT )
    add_string( "openal-audio-device", DEFAULT_OPENAL_DEVICE,
                N_("OpenAL Device Name"), NULL, false )
        change_string_list( ppsz_devices, ppsz_devices_text, FindDevicesCallback )
        change_action_add( FindDevicesCallback, N_("Refresh list") )
    add_float("speaker-width", 100., N_("Speaker Width"), N_("Pick Speaker Width"), true);
    add_float("speaker-height", 100., N_("Speaker Height"), N_("Pick Speaker Height"), true);
    add_integer_with_range("crossfeed-level", 1, 0, 6, N_("CrossFeed Level"), N_("CrossFeed Level for Bs2B plugin"), true);
    add_shortcut( "openal", "openal" )
    set_callbacks( OpenAudio, CloseAudio )
vlc_module_end ()

/*****************************************************************************
 * OpenAudio: open the audio device
 *****************************************************************************
 * This function opens and setups Direct Sound.
 *****************************************************************************/
static int OpenAudio( vlc_object_t *p_this )
{
	aout_instance_t * p_aout = (aout_instance_t *) p_this;
	p_aout->output.output.i_format = VLC_CODEC_FL32;
	p_aout->output.output.i_rate = 44100;

	// Allocate structure
	aout_sys_t *p_sys = malloc( sizeof( *p_sys ) );
	if( unlikely(p_sys == NULL) )
        	return VLC_ENOMEM;
	p_aout->output.p_sys = p_sys;

	//get input parameters
	float spatWidth = var_CreateGetFloat(p_this, "speaker-width");
	float spatHeight = var_CreateGetFloat(p_this, "speaker-height");
	int bs2bLevel = var_CreateGetInteger(p_this, "crossfeed-level");
	//openal positional parameters
	float pos[3] = {0,10,0};
	float dir[6] = {0,0,1,0,-1,-1};
	float speakerPosBase[3] = {0,0,0};
	float speakerPos[MAX_CHANS][3] = 
	{
		{-spatWidth, -spatHeight, 0},
		{spatWidth, -spatHeight, 0},
	};
	//openal variables
	ALCdevice *dev = NULL;
	ALCcontext *ctx = NULL;
	ALCint frequency = 0;
	ALenum err;
	msg_Dbg(p_aout, "Output rate: %d", p_aout->output.output.i_rate);
	ALCint attrs[] = {ALC_FREQUENCY, p_aout->output.output.i_rate, 0, 0};

	char* devInputName =var_InheritString( p_aout, "openal-audio-device" ); 
	dev = alcOpenDevice(devInputName);
	if(!dev)
	{
		msg_Err( p_aout, "cannot open OpenAL Device" );
		return VLC_ENOMEM;
	}
	ctx = alcCreateContext(dev, attrs);
	alcMakeContextCurrent(ctx);
	alListenerfv(AL_POSITION, pos);
	alListenerfv(AL_ORIENTATION, dir);
	alGenSources(MAX_CHANS, source);
	err = alGetError();
	if(err != AL_NO_ERROR)
	{
		msg_Err(p_aout, "OpenAL could not create sources: %d", err);
	}

	alGenBuffers(NUM_BUF, buffers1);
	err = alGetError();
	if(err != AL_NO_ERROR)
	{
		msg_Err(p_aout, "OpenAL Could not create buffers: %d", err);
	}
	alGenBuffers(NUM_BUF, buffers2);
	err = alGetError();
	if(err != AL_NO_ERROR)
	{
		msg_Err(p_aout, "OpenAL Could not create buffers: %d", err);
	}
	int i;
	for(i = 0; i < MAX_CHANS; i++)
	{
		alSourcefv(source[i], AL_POSITION, speakerPos[i]);
		alSource3f(source[i], AL_VELOCITY, 0, 0, 0);
		alSourcei(source[i], AL_SOURCE_RELATIVE, AL_TRUE);
	}
	alcGetIntegerv(dev, ALC_FREQUENCY, 1, &frequency);
	msg_Dbg(p_aout, "Openal Dev freq: %d", frequency);
	p_aout->output.p_sys->freq = frequency;
	p_aout->output.p_sys->i_buffer_size = BUFFER_SIZE;
	//make silence buffer
	p_aout->output.p_sys->silence_buffer = malloc(BUFFER_SIZE);
	memset(p_aout->output.p_sys->silence_buffer, 0, BUFFER_SIZE);
	//hardcode nbsamples, whats better alternative?
	p_aout->output.i_nb_samples = 1024;
	aout_VolumeSoftInit(p_aout);
	p_aout->output.pf_play = Play;
	
	//hardcode period time to test ALSA timing
//	p_sys->i_alsa_period_time = 23219;
	p_sys->start_date = 0;
	vlc_sem_init(&p_sys->wait, 0);

	//Create thread that will send output buffer through OpenAL
	vlc_atomic_set(&p_aout->output.p_sys->abort, 0);
	if(vlc_clone(&p_sys->thread, OpenALThread, p_aout, VLC_THREAD_PRIORITY_OUTPUT))
	{
		msg_Err(p_aout, "cannot  create OpenAL Thread");
		vlc_sem_destroy(&p_sys->wait);
		return VLC_EGENERIC;
	}
	return VLC_SUCCESS;
}

/*****************************************************************************
 * Play: method description here
 *****************************************************************************/
static void PlayIgnore(aout_instance_t *p_aout)
{
	(void) p_aout;
}
static void Play( aout_instance_t *p_aout )
{
	p_aout->output.pf_play = PlayIgnore;
	//fill with silence originally
	int i, j;
	ALenum err;
	for(i = 0; i < NUM_BUF; i++)
	{
		alBufferData(buffers1[i], openalFormat, p_aout->output.p_sys->silence_buffer, p_aout->output.p_sys->i_buffer_size, p_aout->output.p_sys->freq);
		err = alGetError();
		if(err != AL_NO_ERROR)
			msg_Dbg(p_aout, "Openal could not buffer silence. Buffer: %d Error: %d", buffers1[i],err);

		alBufferData(buffers2[i], openalFormat, p_aout->output.p_sys->silence_buffer, p_aout->output.p_sys->i_buffer_size, p_aout->output.p_sys->freq);
		err = alGetError();
		if(err != AL_NO_ERROR)
			msg_Dbg(p_aout, "Openal could not buffer silence. Buffer: %d Error: %d", buffers2[i],err);
	}

	alSourceQueueBuffers(source[0], NUM_BUF, buffers1);
	err = alGetError();
	if(err != AL_NO_ERROR)
		msg_Dbg(p_aout, "Openal could not queue silence to first source. Error: %d", err);		
	alSourceQueueBuffers(source[1], NUM_BUF, buffers2);
	err = alGetError();
	if(err != AL_NO_ERROR)
		msg_Dbg(p_aout, "Openal could not queue silence to second source. Error: %d", err);

	alSourcePlayv(MAX_CHANS, &source);
	err = alGetError();
	if(err != AL_NO_ERROR)
		msg_Dbg(p_aout, "Openal could not begin first play. Error: %d", err);

	p_aout->output.p_sys->start_date = aout_FifoFirstDate(p_aout, &p_aout->output.fifo);
	sem_post(&p_aout->output.p_sys->wait);
}

static void* OpenALThread(void *data)
{
 	aout_instance_t *p_aout = data;
	
	vlc_sem_wait(&p_aout->output.p_sys->wait);
	mwait(p_aout->output.p_sys->start_date - AOUT_PTS_TOLERANCE / 4);
	for(;;)
		OpenALBuffer(p_aout);

	assert(0);
}

static void OpenALBuffer(aout_instance_t *p_aout)
{
	ALint state;
	mtime_t next_date = mdate();
	int cancel = vlc_savecancel();
	block_t *p_buffer;
	int i,j,k,len;
	ALuint tmpBuffer[MAX_CHANS];
	int numProcessedBySource[MAX_CHANS];
	int minNumProcessed = 0;
	ALenum err;
	float posX[2], posY[2], posZ[2];
	float posCoord[MAX_CHANS][3];
	while(!vlc_atomic_get(&p_aout->output.p_sys->abort))
	{
		for(i = 0; i < MAX_CHANS; i++)
		{
			alGetSourcei(source[i], AL_BUFFERS_PROCESSED, &numProcessedBySource[i]);
		}
		minNumProcessed = TakeMin(numProcessedBySource);
		if(minNumProcessed <= NUM_BUF/2)
			continue;
		if(next_date < mdate()) next_date = mdate();
		while(minNumProcessed--)
		{
			p_buffer = aout_OutputNextBuffer( p_aout, next_date, false );
			if(!p_buffer)
			{
			        next_date = aout_FifoFirstDate( p_aout, &p_aout->output.fifo );
			        mwait( next_date - AOUT_PTS_TOLERANCE/4);
			        next_date = mdate();
			        p_buffer = aout_OutputNextBuffer( p_aout, 
			                     next_date,
			                     false);	
				if(!p_buffer)
				{
					msg_Dbg(p_aout, "In Pause");
					continue;
				}
			}
			if(p_buffer)
			{
				next_date+=p_buffer->i_buffer;
			}
			//gotta split buffer by channel, then do this
			for(i = 0; i < MAX_CHANS; i++)
			{
				//fix MAX_CHANS
				/*int chBuffer[p_buffer -> i_buffer/2];
				for(j=0,k=i; k < p_buffer->i_buffer; j++, k+= MAX_CHANS)
				{
					chBuffer[j] = p_buffer->p_buffer[k]; 
				}*/
				alSourceUnqueueBuffers(source[i], 1, &tmpBuffer[i]);
				if(alGetError() != AL_NO_ERROR)
				{
					msg_Dbg(p_aout, "Openal could not unqueue buffer. Channel: %d Tmp: %d", i, tmpBuffer[i]);
				}
				alBufferData(tmpBuffer[i], openalFormat, p_buffer->p_buffer, p_buffer->i_buffer, p_aout->output.p_sys->freq);
				err = alGetError();
				if(err != AL_NO_ERROR)
				{
					if(!p_buffer) msg_Dbg(p_aout, "p_buffer still null");
					msg_Dbg(p_aout, "Openal could not buffer data. Error: %d", err);
				}
				alSourceQueueBuffers(source[i], 1, &tmpBuffer[i]);
				err = alGetError();
				if(err!= AL_NO_ERROR)
				{
					//if(!p_buffer) msg_Dbg(p_aout, "p_buffer still null");
					msg_Dbg(p_aout, "Openal could not requeue buffer. Error: %d", err);
				}
			}
		}
		

//		aout_BufferFree(p_buffer);
		//TODo: convert position logic to rotate rather than move out
		for(i=0; i < MAX_CHANS;i++)
		{
			alGetSource3f(source[i], AL_POSITION, &posX[i], &posY[i], &posZ[i]);
			posCoord[i][0] = posX[i];
			posCoord[i][1] = posY[i];
			posCoord[i][2] = posZ[i];
		}
		posCoord[0][0]--;
		posCoord[1][0]++;
		for(i = 0; i < MAX_CHANS; i++)
		{
			alSourcefv(source[i], AL_POSITION, posCoord[i]);
		}

		alGetSourcei(source[0], AL_SOURCE_STATE, &state);
		if(state != AL_PLAYING)
			alSourcePlayv(MAX_CHANS,&source);
		
	}


	vlc_restorecancel(cancel);
	
}

static int TakeMin(int numArray[])
{
	int min = numArray[0];
	int i;
	for(i = 0; i < MAX_CHANS; i++)
	{
		if(numArray[i] < min)
			min = numArray[i];
	}

	return min;
}
/*****************************************************************************
 * CloseAudio: close the audio device
 *****************************************************************************/
static void CloseAudio( vlc_object_t *p_this )
{
	msg_Dbg(p_this, "Openal CloseAudio");
	aout_instance_t *p_aout = (aout_instance_t *)p_this;
	struct aout_sys_t * p_sys = p_aout->output.p_sys;

	//kill while loop in OpenALThread
	vlc_atomic_set(&p_sys->abort, 1);

	ALCcontext *ctx = alcGetCurrentContext();
	ALCdevice *dev = alcGetContextsDevice(ctx);
	alcMakeContextCurrent(NULL);
	alcDestroyContext(ctx);
	alcCloseDevice(dev);

	/* Make sure that the thread will stop once it is waken up */
	vlc_cancel( p_sys->thread );
	vlc_join( p_sys->thread, NULL );
	vlc_sem_destroy( &p_sys->wait );
	
	free(p_sys);
}
/*****************************************************************************
 * FindDevicesCallBack: used to populate the devices dropdown
 *****************************************************************************/
static int FindDevicesCallback( vlc_object_t *p_this, char const *psz_name,
                               vlc_value_t newval, vlc_value_t oldval, void *p_unused )
{
	module_config_t *p_item;
	(void)newval;
	(void)oldval;
	(void)p_unused;

	p_item = config_FindConfig( p_this, psz_name );
	if( !p_item ) return VLC_SUCCESS;

	/* Clear-up the current list */
	if( p_item->i_list )
	{
		int i;
		/* Keep the first entry */
		for( i = 1; i < p_item->i_list; i++ )
		{
			free( (char *)p_item->ppsz_list[i] );
			free( (char *)p_item->ppsz_list_text[i] );
			msg_Dbg(p_this, "FindDeviceCallbackBegin");
		}
		/* TODO: Remove when no more needed */
		p_item->ppsz_list[i] = NULL;
		p_item->ppsz_list_text[i] = NULL;
	}
	p_item->i_list = 1;

	GetDevices( p_this, p_item );

	/* Signal change to the interface */
	p_item->b_dirty = true;

	return VLC_SUCCESS;
}

static void GetDevices (vlc_object_t *obj, module_config_t *item)
{
	const char* list = alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
	char* devName;
	char* devDesc;

	if(!list || *list == '\0')
		return VLC_SUCCESS;
	else do {
		asprintf(&devName, "%s", list);
		asprintf(&devDesc, "%s", list);
		if (item != NULL)
		{
			msg_Dbg(obj, "Adding Device to list");
			item->ppsz_list = xrealloc(item->ppsz_list, (item->i_list + 2) * sizeof(char *));
			item->ppsz_list_text = xrealloc(item->ppsz_list_text, (item->i_list + 2) * sizeof(char *));
			item->ppsz_list[item->i_list] = devName;
			item->ppsz_list_text[item->i_list] = devDesc;
			item->i_list++;
			msg_Dbg(obj, "Added Device to list");
		}
		
		list += strlen(list) + 1;
	} while(*list != '\0');

    	if (item != NULL)
	{
		item->ppsz_list[item->i_list] = NULL;
		item->ppsz_list_text[item->i_list] = NULL;
	}
}
